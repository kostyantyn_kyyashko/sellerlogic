<?php
/**
 * Created by PhpStorm.
 * User: konst20
 * Date: 27.08.2017
 * Time: 21:27
 */
require_once "/var/www/sl.nottes.net/gmonitor/gearman_includes.php";

$worker = new GearmanWorker();
$worker->addServer('127.0.0.1');

global $client;
$client = new GearmanClient();
$client->addServer('127.0.0.1');


$worker->addFunction('product', 'product');
function product(GearmanJob $job)
{
    $db = new Amazon_Db();

    $data = unserialize($job->workload());
    $product_id = $data['product_id'];
    $parser = new Parser();
    $url = 'https://www.amazon.de';
    $cookies = $parser->web_page_get($url)['cookies'];
    sleep(1);
    $url = $parser->product_url_generate($product_id);
    $html = $parser->web_page_get($url, $cookies)['content'];
    $parser->xpath_create($html);
    $xpath = [
        'title' => '//*[@id="productTitle"]',
        'manufacturer' => '//*[@id="brand"]',
        'price' => '//*[@id="priceblock_ourprice"]',
        'offers_new_count' => '//*[@id="olp_feature_div"]/div/span/a',
        'offers_used_count' => '//*[@id="olp_feature_div"]/div/span[2]/a',
    ];
    $product = [];
    $product['product_id'] = $product_id;
    $product['title'] = $parser->parse_single($xpath['title']);
    $product['manufacturer'] = $parser->parse_single($xpath['manufacturer']);
    $product['price'] = $parser->parse_single($xpath['price']);
    $product['offers_new_count'] = $parser->parse_single($xpath['offers_new_count']);
    $product['offers_used_count'] = $parser->parse_single($xpath['offers_used_count']);
    $product['seller_in_the_buybox'] = '';
    if ($product['title '] && mb_strlen($product['title']) > 5) {
        $result = $product['title'];
    }
    else {
        $db->html_insert($html);
    }
    //file_put_contents('/var/www/_dl/amazon.txt', $product_id . ' | ' . $product['price'] . PHP_EOL, FILE_APPEND);

    $db->product_insert($product);
    return;
}


while($worker->work());

