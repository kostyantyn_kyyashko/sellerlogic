<?php
/**
 * Created by PhpStorm.
 * User: konst20
 * Date: 21.08.2017
 * Time: 4:58
 */

class Phantom_old {

    /**
     * Код продукта
     * @var
     */
    private $product;

    /**
     * Что будем парсить
     * @var
     */
    private $parse_variant;

    /**
     * Путь к JS обработчику относительно корня проекта
     * @var string
     */
    public $path_to_js = '/var/www/sl.nottes.net/core/js/parse.js';

    /**
     * Phantom constructor.
     * @param $product - код продукта, например B00VYAWN5U
     * @param $parse_variant - что парсим, см. all_parse_variants(). По этой переменной
     *                          вызываются коллбэки в JS и в этом классе
     * @param int $start_index - для многостраничных вариантов, для парсинга страниц. Обычно кратно 10-ти
     * @throws Exception - бдыщь!!!
     */
    public function __construct($product, $parse_variant, $start_index = 0)
    {
        $this->product = $product;
        $this->parse_variant = $parse_variant;
        $this->start_index = $start_index;
    }

    /**
     * На случай парсинга Амазон в других странах / на др. доменах
     * @var string
     */
    public $root_url = 'https://www.amazon.de';

    /**
     * опции запуска PhantomJS, см. доки. В версии 2.1.1 SSL уже нормально работает, пинать фантома (пока) не нужно
     * @return string
     */
    private function phantomjs_options()
    {
        $options = [
/*            '--ignore-ssl-errors=true',
            '--ssl-protocol=any',
            '--web-security=false'*/
        ];
        return implode(' ', $options);
    }

    /**
     *
     * @param int $start_index
     * @return mixed
     */
    private function url ($start_index = 0)
    {
        $url_variants = [
            'product' => "{$this->root_url}/any/dp/{$this->product}/",
            'offer_new' => "{$this->root_url}/gp/offer-listing/{$this->product}/ref=dp_olp_new?ie=UTF8&condition=new&startIndex={$start_index}",
            'offer_used' => "{$this->root_url}/gp/offer-listing/{$this->product}/ref=dp_olp_used?ie=UTF8&condition=used&startIndex={$start_index}",
        ];
        return $url_variants[$this->parse_variant];
    }

    public function get_phantom_data ()
    {
        $exec_string = <<<txt
phantomjs {$this->phantomjs_options()} {$this->path_to_js} "{$this->url()}" {$this->parse_variant}
txt;
;
        //return ($exec_string);
        return shell_exec($exec_string);
    }



}