<?php
class Parser {

    /**
     * @var DOMXPath
     */
    public $xpath_obj;
    public $proxy;

    /**
     * Пока остановимся на amazon.de
     * @var string
     */
    protected $root_url = 'https://www.amazon.de';

    public function __construct()
    {
        $this->proxy_generate();
    }

    /**
     * generate URL for product
     * @param $product_id
     * @return string
     */
    public function product_url_generate ($product_id)
    {
        return "{$this->root_url}/any/dp/$product_id/";
    }

    /**
     * generate URL for offers
     * @param $product_id
     * @param string $new_used
     * @param int $start_index
     * @return string
     */
    public function offer_url_generate ($product_id, $new_used = 'new', $start_index = 0)
    {
        return "{$this->root_url}/gp/offer-listing/{$product_id}/ref=dp_olp_new?ie=UTF8&condition={$new_used}&startIndex={$start_index}";
    }

    /**
     *
     * @param $array
     * @return mixed
     */
    private function get_random_element($array)
    {
        return $array[mt_rand(0, count($array) - 1)];
    }

    /**
     * @return mixed
     */
    private function useragent_generate()
    {
        $rnd1000 = mt_rand(1000, 2000);
        $rnd10 = mt_rand(11,99);
        $rnd1 = mt_rand(9,11);
        $useragent = [];
        $useragent[] = "Mozilla/5.0 Macintosh; Intel Mac OS X 10_10_3 AppleWebKit/{$rnd1000}.{$rnd10} KHTML, like Gecko Chrome/44.0.2376.0 Safari/537.36 OPR/31.0.1857.0";
        $useragent[] = "Mozilla/5.0 X11; Linux i686; rv:40.0 Gecko/{$rnd1000} Firefox/40.0";
        $useragent[] = "Mozilla/5.0 Windows NT 10.0; WOW64; rv:40.0 Gecko/{$rnd1000} Firefox/40.0";
        $useragent[] = "Mozilla/5.0 Macintosh; Intel Mac OS X 10_10_2 AppleWebKit/{$rnd1000}.{$rnd10} KHTML, like Gecko Chrome/40.0.2214.38 Safari/537.36";
        $useragent[] = "Mozilla/5.0 Windows NT 6.1; WOW64 AppleWebKit/{$rnd1000}.{$rnd10} KHTML, like Gecko Chrome/46.0.2490.71 Safari/537.36";
        $useragent[] = "Mozilla/5.0 Windows NT 6.1; WOW64 AppleWebKit/{$rnd1000}.{$rnd10} KHTML, like Gecko Chrome/51.0.2704.103 Safari/537.36";
        $useragent[] = "Mozilla/5.0 compatible; MSIE {$rnd1}.0; Windows NT 6.1; Trident/6.0";
        $useragent[] = "Mozilla/5.0 compatible; MSIE {$rnd1}.0; Windows NT 6.1; WOW64; Trident/6.0";
        $useragent[] = "Mozilla/5.0 compatible; MSIE {$rnd1}.0; Windows NT 6.2; Win64; x64; Trident/6.0";
        $useragent[] = "Mozilla/5.0 IE 11.0; Windows NT 6.3; Trident/7.0; .NET4.0E; .NET4.0C; rv:{$rnd1}.{$rnd10} like Gecko";

        return $this->get_random_element($useragent);
    }

    public $proxy_list_file = '/var/www/sl.nottes.net/core/proxy.txt';

    /**
     * generate random proxy from list (file)
     */
    public function proxy_generate ()
    {
        $proxy = file_get_contents($this->proxy_list_file);
        $proxy = explode(PHP_EOL, $proxy);
        $proxy = $this->get_random_element($proxy);
        $this->proxy = trim($proxy);

    }

    /**
     * @param $url
     * @return mixed
     */
    public function get_curl_data($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_USERAGENT, $this->useragent_generate());
        curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //if (curl_getinfo($ch)['http_code'] != 200) return false;
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /**
     * get full data of web page, include header, coockies etc
     * @param $url
     * @param string $cookies_in
     * @param mixed $proxy - format 1.2.3.4:56 or false
     * @return mixed
     */
    public function web_page_get($url, $cookies_in = '', $proxy = false){
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_COOKIE         => $cookies_in,
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        if ($proxy) {
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
        }
        $rough_content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
        preg_match_all($pattern, $header_content, $matches);
        $cookies_out = implode("; ", $matches['cookie']);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['headers']  = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookies_out;
        return $header;
    }

    /**
     * @param $cookies_string
     * @return array
     */
    private function cookies_string_to_array ($cookies_string)
    {
        $cookies = explode(';', $cookies_string);
        $out = [];
        foreach ($cookies as $c) {
            $c = explode('=', trim($c));
            $out[$c[0]] = $c[1];
        }

        return $out;
    }

    /**
     * get URL - src attribute - of captcha image from captcha page
     * @param $html
     * @return bool|string
     */
    public function captcha_src_get ($html)
    {
        $html = explode('https://images-na.ssl-images-amazon.com/captcha/', $html);
        if (!isset($html[1])) return false;
        $captcha_source = explode('.jpg', $html[1])[0];
        return 'https://images-na.ssl-images-amazon.com/captcha/' . $captcha_source . '.jpg';
    }

    /**
     * fill xpath property
     * @param $html
     */
    public function xpath_create ($html)
    {
        $dom = new DOMDocument();
        $dom->loadHTML($html);
        $this->xpath_obj = new DOMXPath($dom);
    }

    /**
     * Get content of single element of page, e.g. titel, manufacturer etc.
     * @param $xpath
     * @return string
     */
    public function parse_single ($xpath)
    {
        $raw = $this->xpath_obj->query($xpath)->item(0)->nodeValue;
        return trim($raw);
    }

    /**
     * wrap of captcha solve service
     * @param $img_base64_encoded
     * @return mixed
     * @throws Exception
     */
    public function captcha_solve ($img_base64_encoded)
    {
        require Config::$root_path . "/captcha/anticaptcha.php";
        require Config::$root_path . "/captcha/imagetotext.php";
        $api = new ImageToText();
        $api->setVerboseMode(false);
        $api->setKey("0d2a27a4eee64f64bc233c784c34823d");
        $api->setFileBase64($img_base64_encoded);
        if (!$api->createTask()) {
            throw new Exception('not create task');
        }
        $taskId = $api->getTaskId();
        if (!$api->waitForResult()) {
            throw new Exception('could not solve captcha // ' . $api->getErrorMessage());
        } else {
            return $api->getTaskSolution();
        }

    }



}