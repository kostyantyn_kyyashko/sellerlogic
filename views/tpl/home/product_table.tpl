{if !$send_status}
    <div style="border: 1px solid grey; width: 400px; padding: 10px; border-radius: 3px;">
        <form action="" method="post" enctype="multipart/form-data" id="pform">
            <input class="form-control" type="file" name="plist" style="width: auto;">
            <button type="submit" class="form-control btn btn-info" style="width: auto; margin-top: 5px; margin-left: 15px;">Отправить для парсинга</button>
        </form>
    </div>
{else}
    <h4>Отправлено в очередь {$send_status} товаров</h4>
{/if}
<div>
    <h4>
        <a href = '/parse' target="_blank" style="margin-right: 5px;" class="btn btn-success">
            Добавить задачи в очередь
        </a>
        <a href = '/delete' class="btn btn-danger">
            Очистить таблицу в БД
        </a>
        <a href = '/gmonitor/gmonitor.php' class="btn btn-default" target="_blank">
            Управление очередью
        </a>

    </h4>
</div>
<table class="table table-bordered table-striped table-condensed">
    <thead>
    <tr>
        <th>Product Id</th>
        <th>title</th>
        <th>manufacturer</th>
        <th>price</th>
        <th>Offers New</th>
        <th>Offers Used</th>
        <th>Other Sellers</th>
    </tr>
    </thead>
    <tbody>
    {foreach $product as $p}
        <tr>
            <td>{$p.product_id}</td>
            <td>{$p.title}</td>
            <td>{$p.manufacturer}</td>
            <td>{$p.price}</td>
            <td>{$p.offers_new_count}</td>
            <td>{$p.offers_used_count}</td>
            <td>{implode(', ', json_decode($p.seller_in_the_buybox, 1))}</td>
        </tr>
        {foreachelse}
    {/foreach}
    </tbody>
</table>