<?php
class Amazon_Db extends Db {

    public function product_insert ($product)
    {
        $sql = <<<sql
INSERT IGNORE INTO 
product
SET
product_id = :product_id, 
title = :title, 
manufacturer = :manufacturer, 
price = :price, 
offers_new_count = :offers_new_count, 
offers_used_count = :offers_used_count,
seller_in_the_buybox = :seller_in_the_buybox
sql;
        $data = [
            'product_id' => $product['product_id'],
            'title' => $product['title'],
            'manufacturer' => $product['manufacturer'],
            'price'  => $product['price'],
            'offers_new_count'  => $product['offers_new_count'],
            'offers_used_count'  => $product['offers_used_count'],
            'seller_in_the_buybox'  => json_encode($product['seller_in_the_buybox']),

        ];
        $this->sql_prepare_and_execute($sql, $data);
    }

    public function product_select_all()
    {
        $sql = "SELECT * FROM product";
        return $this->select_all($sql);
    }

    public function delete_all()
    {
        $sql = "DELETE FROM product";
        $this->sql_prepare_and_execute($sql);
    }

    public function html_insert($html)
    {
        $sql = <<<sql
INSERT INTO test
set html = :html
sql;
        $data = [
            'html' => $html,
        ];

        $this->sql_prepare_and_execute($sql, $data);
    }

    public function insert_ip ($ip)
    {
        $sql = <<<sql
INSERT INTO ip
SET 
ip = :ip,
stat = 1
ON DUPLICATE KEY UPDATE 
stat = stat+1
sql;
        $data = [
            'ip' => $ip
        ];
        $this->sql_prepare_and_execute($sql, $data);
    }

}