<?php
class Gearman_Logmaker{

    private static function exceptions()
    {
        $ex = [
            'already loaded',
            'Smarty'
        ];
        return $ex;
    }

    /**
     * Вставляем сообщение в лог
     * @param $operation
     */
    public static function save_log($operation){
        foreach(self::exceptions() as $ex){
            if(strstr($ex, $operation)){
                return;
            }
        }
        $db = new Gearman_Db();
        $db->log_insert($operation);
    }

}
 
