<?php
class home extends Controller{

    public function run($req){
        $tpl = new Tpl_Obj();
        $db = new Amazon_Db();

        $tpl->display('common/page_header.tpl');
        $product = $db->product_select_all();
        $tpl->assign('product', $product);
        $send_status = 0;
        if (isset($_FILES['plist'])) {
            $list = file_get_contents($_FILES['plist']['tmp_name']);
            $list = explode(PHP_EOL, $list);
            $client = new GearmanClient();
            $client->addServer('localhost');
            foreach ($list as $id) {
                $client->doBackground('product', serialize(['product_id' => trim($id)]));
                $send_status++;
            }
        }
        $tpl->assign('send_status', $send_status);
        $tpl->display('home/product_table.tpl');
        $tpl->display('common/page_footer.tpl');
    }

    public function parse()
    {
        $client = new GearmanClient();
        $client->addServer('127.0.0.1');
        $products = <<<text
B01AGGJ44K
B00VYAWIRS
B001OUS8RC
B003LNLPQ6
B00FSBC9VO
B01LXLFF6H
B00VYAWLZW
B003LNLPJS
B00D145OYY
B01ARRTHUE
B005IW9Q1E
B001BJKO3M
B01LXMG788
B00UKZ1NBK
B01AGDX0OI
B00HQAOOYK
B01LXOQ1KJ
B000NJUWME
B0050OTQ42
B009KFST90
B000S5JP30
B00VB2ISDM
B0030E5WYW
B005IW9QAK
B01MU50WZ4
B001CL8CO2
B0187JGP1I
B000FFR5KI
B01E3R7T9K
B004UV1I0Q
B000KJNZ7Q
B00VYAWN5U
B000J69O8E
B000FA5E7O
B005LU5U4K
B0014E4MWS
B00ET01FR4
B01N4P0R51
B015U51ONG
B01FQHE25U
B000JTKDCW
B01MQQQKGU
B00FAWORFC
B003Y5KND2
B000KJOC78
B0019TWRSO
B005IUSC4S
B01FSGVN4M
B00CUDEKUQ
B00VYAWKJY
text;
        $products = explode(PHP_EOL, $products);

        foreach ($products as $id) {
            $client->doBackground('product', serialize(['product_id' => trim($id)]));
        }
        header('Location:http://sl.nottes.net/gmonitor/gmonitor.php');
    }

    public function delete ()
    {
        $db = new Amazon_Db();
        $db->delete_all();
        header('Location: /');
    }

    public function ip ()
    {
        $ip = $_SERVER['SERVER_ADDR'];
        $db = new Amazon_Db();
        $db->insert_ip($ip);
        $html = <<<html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
ok
</body>
</html>
html;
echo $html;
    }

    public function ip_stat ()
    {
        $client = new GearmanClient();
        $client->addServer('127.0.0.1');
        for ($i=0; $i<1000; $i++) {
            $client->doBackground('ip', mt_rand(0, 1000000));
        }

    }

    public function ft ()
    {
        $phantom = new Phantom('sl.nottes.net/ip', 'ip');
        $adata = $phantom->get_phantom_data();
        echo $adata;
    }



}