<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="icon" href="{$smarty.const.PROJECT_URL}/views/img/favicon.ico" type="image/x-icon" />

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/run_prettify.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

    <link type="text/css" rel="stylesheet" href="{$smarty.const.PROJECT_URL}/views/css/style.css"/>

    <script type="text/javascript" src="{$smarty.const.FULL_URL_TO_FW}/common_views/js/jquery.cookie.js"></script>


    {*Клиентские яваскрипты - массивом*}
    {if isset($js)}
        {foreach $js as $client_js}
        <script type="text/javascript" src="{$smarty.const.PROJECT_URL}/views/js/{$client_js}"></script>
        {foreachelse}
        {/foreach}
    {/if}

    {*Клиентские стили - кроме style.css, если есть конечно*}
    {if isset($css)}
        {foreach $css as $client_css}
            <link rel="stylesheet" type="text/css" href="{$smarty.const.PROJECT_URL}/views/css/{$client_css}"/>
        {foreachelse}
        {/foreach}
    {/if}


</head>
<body>
<div class="container">