<?php
/**
 * Created by PhpStorm.
 * User: konst20
 * Date: 20.03.2017
 * Time: 5:37
 */
require_once 'includes.php';

$GLOBALS['cli'] = true;

if (!$argv) {
    echo "Usage: php cli.php <controller> <method>";
    die();
}

$class = $argv[1];
if (class_exists($class)) {
    $controller = new $class;
}
else {
    echo "Controller $class not exists";
    die();
}


$method = $argv[2];
if (method_exists($controller, $method)) {
    $controller->$method();
}
else {
    echo "Method $method not defined in controller $class";
    die();
}
/*$import = [
    [
        'message_id' => $message1_id,
        'case_id' => $case1_id,
        'message' => $message1_text,
    ],
    [
        'message_id' => $message2_id,
        'case_id' => $case1_id,
        'message' => $message2_text,
    ],
    [
        'message_id' => $message3_id,
        'case_id' => $case2_id,
        'message' => $message3_text,
    ],
    [
        'message_id' => $message4_id,
        'case_id' => $case2_id,
        'message' => $message4_text,
    ]
];*/
