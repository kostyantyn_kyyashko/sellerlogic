/**
 * Created by konst20 on 21.08.2017.
 */

var page = require('webpage').create(),
    system = require('system'),
    url = system.args[1],
    action = system.args[2];

page.settings.loadImages = false;
page.onResourceRequested = function (requestData, request) {
    if ((/http:\/\/.+?\.css$/gi).test(requestData['url'])) {
        request.abort();
    }
    if (
        (/\.doubleclick\./gi.test(requestData['url'])) ||
        (/\.pubmatic\.com$/gi.test(requestData['url'])) ||
        (/yandex/gi.test(requestData['url'])) ||
        (/google/gi.test(requestData['url'])) ||
        (/gstatic/gi.test(requestData['url']))
    ) {
        request.abort();
    }
};

function get_random_elemen(array) {
    return array[Math.floor(Math.random() * array.length)];
}

function useragent_create() {
    var useragent = [];
    useragent.push('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2376.0 Safari/537.36 OPR/31.0.1857.0');
    useragent.push('Mozilla/5.0 (X11; Linux i686; rv:40.0) Gecko/20100101 Firefox/40.0');
    useragent.push('Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0');
    useragent.push('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36');
    useragent.push('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36');
    useragent.push('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36');
    useragent.push('Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)');
    useragent.push('Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)');
    useragent.push('Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Win64; x64; Trident/6.0)');
    useragent.push('Mozilla/5.0 (IE 11.0; Windows NT 6.3; Trident/7.0; .NET4.0E; .NET4.0C; rv:11.0) like Gecko');
    useragent.push('Mozilla/5.0 (IE 11.0; Windows NT 6.3; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko');

    //return get_random_elemen(useragent);
    return get_random_elemen(useragent);
}

function screen_resolution_create() {
    var screen = [];
    screen.push([1366,768]);
    screen.push([1920,1080]);
    screen.push([1440,900]);
    screen.push([1680,1050]);
    screen.push([1280,1024]);
    screen.push([1280,720]);
    screen.push([1280,800]);
    screen.push([1600,900]);
    screen.push([1536,864]);
    return get_random_elemen(screen);
}
var screen_resolution = screen_resolution_create();
page.viewportSize = {
    width: screen_resolution[0],
    height: screen_resolution[1]
};

function accept_create() {
    var accept = [];
    accept.push('text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8');
    accept.push('text/html,application/xhtml+xml,application/xml;q=0.8,*/*;q=0.9');
    accept.push('text/html,application/xhtml+xml,application/xml;q=0.7,*/*;q=0.8');
    accept.push('text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
    accept.push('text/html,application/xhtml+xml,application/xml;q=0.8,image/webp,image/apng,*/*;q=0.9');
    accept.push('text/html,application/xhtml+xml,application/xml;q=0.7,image/webp,image/apng,*/*;q=0.8');
    accept.push('text/html,application/xhtml+xml,application/xml;q=0.8,image/webp,image/apng,*/*;q=0.9');
    accept.push('text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.7');
    accept.push('text/html,application/xhtml+xml,application/xml;q=0.8,image/webp,image/apng,*/*;q=0.6');
    accept.push('text/html,application/xhtml+xml,application/xml;q=0.9,image/apng,*/*;q=0.8');
    return get_random_elemen(accept);
}

function accept_language_create() {
    var al = [];
    al.push('en-US,en;q=0.6,de;q=0.4');
    al.push('en-US,en;q=0.4,de;q=0.8');
    al.push('en-US,en;q=0.5,de;q=0.7');
    al.push('en-US,en;q=0.4,de;q=0.5');
    al.push('en-US,en;q=0.6,de;q=0.2');
    al.push('en-US,en;q=0.8,de;q=0.6');
    al.push('en-US,en;q=0.5,de;q=0.6');
    al.push('en-US,en;q=0.4,de;q=0.7');
    al.push('en-US,en;q=0.3,de;q=0.8');
    al.push('en-US,en;q=0.4,de;q=0.9');
    return get_random_elemen(al);
}


page.customHeaders = {
    ":host": "amazon.de",
    ":method": "GET",
    ":scheme": "https",
    "accept": accept_create(),
    "accept-language": accept_language_create(),
    "cache-control": "max-age=0",
    "upgrade-insecure-requests": "1",
    "user-agent": useragent_create()
};

page.onError = function (msg, trace) {
/*
    console.log(msg);
    trace.forEach(function (item) {
        console.log('  ', item.file, ':', item.line);
    });
*/
};

page.onAlert = function( msg ) {
/*
    console.log( msg );
*/
};

var session = Math.random().toString(36).replace('0.', '');

var cookie_path;
var localstorage_path;
var cache_path;

/*
var url = 'https://www.amazon.de/HP-Original-Druckerpatrone-Deskjet-Officejet/dp/B00VYAWN5U/';
var url = 'https://www.amazon.de/gp/offer-listing/B00VYAWN5U/ref=dp_olp_new?ie=UTF8&condition=new';
*/
function product() {
    page.open(url, function (status) {
        if (status === 'success') {
            if (page.injectJs('/var/www/sl.nottes.net/jquery.js')) {
                var data = page.evaluate(function() {
                    var page_data = {
                        //title: document.querySelector('#productTitle').textContent.trim()
                        title: $('#productTitle').text().trim(),
                        manufacturer: $('#brand').text().trim(),
                        price: $('#priceblock_ourprice').text().trim(),
                        offers_new_count: $('#olp_feature_div').find('a').eq(0).text(),
                        offers_used_count: $('#olp_feature_div').find('a').eq(1).text()
                    };
                    var seller_in_the_buybox = [];
                    var other_seller_elements = $('.mbcMerchantName');
                    $.each(other_seller_elements, function (index, seller) {
                        seller_in_the_buybox.push($(seller).text().trim())
                    });
                    page_data['seller_in_the_buybox'] = seller_in_the_buybox;
                    return JSON.stringify(page_data);
                });
                console.log(data);
                phantom.exit();
            }
        }
    });
}

function ip() {
    page.open(url, function (status) {
        if (status === 'success') {
            if (page.injectJs('/var/www/sl.nottes.net/jquery.js')) {
                var data = page.evaluate(function() {
                    return $('body').text();
                });
                console.log(data);
                phantom.exit();
            }
        }
    });
}

function offer() {
    page.open(url, function (status) {
        if (status === 'success') {
            if (page.injectJs('/var/www/sl.nottes.net/jquery.js')) {
                var data = page.evaluate(function() {
                    var offer_elements = $('.olpOffer');
                    var final_offers_data = {};
                    $.each(offer_elements, function (index, offer) {
                        var od = {
                            price: $(offer).find('.olpOfferPrice').text().trim(),
                            shipping: $(offer).find('.olpShippingInfo').text().trim(),
                            seller_name: $(offer).find('.olpSellerName').find('a').text().trim(),
                            seller_ID: String($(offer).find('.olpSellerName').find('a').attr('href')).split('seller=')[1],
                            seller_rate: $(offer).find('.olpSellerColumn').find('a').eq(1).text(),
                            seller_url: $(offer).find('.olpSellerName').find('a').attr('href'),
                            seller_condition_url: $(offer).find('.olpSellerColumn').find('a').eq(2).attr('href')
                        };
                        final_offers_data[index] = od;
                    });

                    return JSON.stringify(final_offers_data);
                });
                console.log(data);
                phantom.exit();
            }
        }
    });
}

eval(action + '()');