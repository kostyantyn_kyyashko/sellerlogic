<?php
/**
 * Created by PhpStorm.
 * User: konst20
 * Date: 21.08.2017
 * Time: 4:58
 */

class Phantom {

    /**
     * Адрес страницы, которую будем парсить
     * @var
     */
    private $url;

    /**
     * Что будем парсить
     * Ф-я с таким именем должна быть в файле /core/js/parse.js
     * @var
     */
    private $parse_variant;

    /**
     * Путь к JS обработчику относительно корня проекта
     * @var string
     */
    private $path_to_js = '/var/www/sl.nottes.net/core/js/parse.js';

    /**
     * Здесь будут хранится куки, localstorage и cache для каждой сессий
     * @var string
     */
    private $storagePath = '/var/www/sl.nottes.net/storage';

    private $sessionId;
    private $coockiePath;
    private $localstoragePath;
    private $cachePath;
    private $proxy;

    /**
     * Агрументы см. выше
     * Phantom constructor.
     * @param $url
     * @param $parse_variant
     */
    public function __construct($url, $parse_variant)
    {
        $this->url = $url;
        $this->parse_variant = $parse_variant;
        $this->generateSession();
        $this->generateProxy();
    }

    /**
     * папки для куки и пр.
     * @param $path
     */
    private function stotageDirectoryCreate($path)
    {
        mkdir($path);
        chmod($path, 0777);
    }

    /**
     * Создаем сессию, нужные пути и создаем папки
     */
    private function generateSession ()
    {
        $this->sessionId = md5(mt_rand(0, 1000000));
        $sessionPath = "{$this->storagePath}/{$this->sessionId}";
        //$this->stotageDirectoryCreate($sessionPath);
        mkdir($sessionPath);
        chmod($sessionPath, 0777);


        $this->coockiePath = "{$sessionPath}/cookie";
        //$this->stotageDirectoryCreate($this->coockiePath);
        mkdir($this->coockiePath);
        chmod($this->coockiePath, 0777);


        $this->localstoragePath = "{$sessionPath}/localstorage";
        //$this->stotageDirectoryCreate($this->localstoragePath);
        mkdir($this->localstoragePath);
        chmod($this->localstoragePath, 0777);


        $this->cachePath = "{$sessionPath}/cache";
        //$this->stotageDirectoryCreate($this->cachePath);
        mkdir($this->cachePath);
        chmod($this->cachePath, 0777);

    }

    private static function delTree($dir) {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    private function destroySession ()
    {

    }

    private function generateProxy ()
    {
        $proxy = file_get_contents('/var/www/sl.nottes.net/core/proxy.txt');
        $proxy = explode(PHP_EOL, $proxy);
        $proxy = $proxy[mt_rand(0, count($proxy)-1)];
        $this->proxy = trim($proxy);
    }

    /**
     * опции запуска PhantomJS, см. доки. В версии 2.1.1 SSL уже нормально работает, пинать фантома (пока) не нужно
     * @return string
     */
    private function phantomjs_options()
    {
        $options = [
            "--ignore-ssl-errors=true",
            "--ssl-protocol=any",
            "--web-security=false",
            "--cookies-file={$this->coockiePath}/cookie.txt",
            "--disk-cache=true",
            "--disk-cache-path={$this->cachePath}",
            "--load-images=true",
            "--local-storage-path={$this->localstoragePath}",
            "--proxy={$this->proxy}"
        ];
        return implode(' ', $options);
    }

    public function get_phantom_data ()
    {
        $exec_string = <<<txt
phantomjs {$this->phantomjs_options()} {$this->path_to_js} "{$this->url}" {$this->parse_variant}
txt;
;
        return shell_exec($exec_string);
    }



}