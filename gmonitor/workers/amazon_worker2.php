<?php
/**
 * Created by PhpStorm.
 * User: konst20
 * Date: 27.08.2017
 * Time: 21:27
 */
require_once "/var/www/sl.nottes.net/gmonitor/gearman_includes.php";
require_once "/var/www/sl.nottes.net/core/simple_html_dom.php";

$worker = new GearmanWorker();
$worker->addServer('127.0.0.1');

global $client;
$client = new GearmanClient();
$client->addServer('127.0.0.1');

global $gdb;
$gdb = new Gearman_Db();

$worker->addFunction('product', 'product');
function product(GearmanJob $job)
{
    global $client;
    global $gdb;
    $data = unserialize($job->workload());
    $product_id = $data['product_id'];
    $parser = new Parser();
    $url_amz = $parser->product_url_generate($product_id);
    $url = 'http://PhantomJScloud.com/api/browser/v2/ak-cdkhc-kfvpq-d2aqd-xpwwv-0ab11/';
    $payload = '
{
	"url":"' . $url_amz . '",
	"renderType":"html"
}
        ';
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/json\r\n",
            'method'  => 'POST',
            'content' => $payload
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    $db = new Amazon_Db();
    $db->html_insert($result);
    if ($result === FALSE) { /* Handle error */ }
    return;
}

$worker->addFunction('new_offers_data', 'new_offers_data');
function new_offers_data(GearmanJob $job)
{
    global $client;
}

$worker->addFunction('used_offers_data', 'used_offers_data');
function used_offers_data(GearmanJob $job)
{
    global $client;
}

while($worker->work());

